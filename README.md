# NodeJs Fastify REST API

## Summary
This is a project with the goal to demostrate my skills as a full stack developer. The project stack is Javascript with NodeJs as engine, using Typescript and Fastify as the Node Framework to choose.

The project assumes you have a ready-to-use mongodb server. You can read how to connect it later in the Getting started section bellow.

### Context
It has being a while since i developed my last backend server, so this project was extremely challenging for me as it was not only the project technical requirement but also, adding the complexity of getting in touch with a lot of technologies I had very dark in my head or even, I didn't known before this project. I found this context very important to properly understan the output quality when reading.

I tracked my time while I was doing this to offer a transparent window so the reader can understand how much effort I put behind this. The Clockify report it's here:
https://app.clockify.me/shared/64f221662427015160fe9125

Thanks for reading this and I hope you apretiate the project.
I will share the link of the production site bellow.

## Getting started

After cloning this project, you'll need to run
```
npm install
```

### Second Step:
You'll need to create a .env file in the root directory, you will find a .env.example to know what variables you need to setup before run the project

### Third Step:
you only need to run
```
npm run dev
```
to start coding and, if you want to create a deploy build, only need to run
```
npm run build
```

## Development Doubth
### Authorization

- CORS configuration
- use a randomizer dependency to create the secret string for jws cofiguration
- Token Lifecycle:
    - Revoke Tokens
    - Token Lifetime
    - Token Refresh
- User Roles
    - Administrators: users who can execute command routes
    - Viewer: user who can only create reviews documents and query all models except users
- User authorization utils:
    - user signup
    - password creation
    - password reset

### Overall Features

- include unit testing configuration with Jest.js
- create a seeding strategy for testing
- include a file storage system to properly save the movie’s images and the platform’s icons
- capture errors with a personalized or Fastify documented way to retrieve friendly error messages and status code handling properly
- include static variables in generic model to prevent using dangerous literal strings for collection’s name reference in queries:
    - e.g.: 
    ```
    // instead of creating a lookup like:
    {
        from: “Users”
    } 
    // do something like:
    {
        from: UserQuery.useCollectionName()
    }
    ```
- include the following types for an improved typing experience:
    - jwt token payload decoded data
    - Request decorator with user session data
    - create separate Entities CRUD types, collection/aggregation results cursor type

### Movie’s Model

- search by slug
- Use salt or nonce for slug creation for uniqueness
- create a collection unique index by slug
- Update the score property with an event-based

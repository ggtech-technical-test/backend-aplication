import UserQuery from "@models/query/UserQuery";
import IUser from "@models/types/user";
import Authentication from "auth/models/Authentication";
import { ILogIn } from "auth/types";
import { FastifyReply, FastifyRequest } from "fastify";

export async function logIn(
  request: FastifyRequest<{
    Body: ILogIn;
  }>,
  reply: FastifyReply
) {
  const tokenModel = new Authentication(request);
  const userModel = new UserQuery(request);
  const user = (await userModel.checkCredentials(
    request.body.email,
    request.body.password
  )) as IUser;
  if (!user) {
    return reply.status(400).send({
      message: "wrong combination of email and password",
    });
  }
  const userObj = await userModel.showUser(user._id!);
  const token = await tokenModel.logIn(user._id!);
  return reply.status(200).send({
    data: { token, user: userObj },
    message: "Welcome Netflix",
  });
}

import { FastifySchema } from "fastify";

export const loginSchema: FastifySchema = {
  body: {
    type: "object",
    required: ["email", "password"],
    properties: {
      email: {
        type: "string",
        maxLength: 50,
      },
      password: {
        type: "string",
        maxLength: 50,
      },
    },
  },
};

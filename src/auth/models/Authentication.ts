import GenericCommandModel from "@generics/GenericCommandModel";
import { IToken } from "auth/types";
import { FastifyRequest } from "fastify";

export default class Authentication extends GenericCommandModel<IToken> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Tokens");
  }

  public async logIn(user_id: string) {
    try {
      const signedTkn = this.server.jwt.sign({ user_id });
      const userObjId = new this.mongo.ObjectId(user_id);
      const collection = this.getCollection();
      await collection?.insertOne({
        _id: new this.mongo.ObjectId(),
        user: userObjId,
        jwt: signedTkn,
      });
      return signedTkn;
    } catch (error) {
      throw new Error(
        `Something came wrong while creating the token for ${user_id}`
      );
    }
  }
}

import IUser from "@models/types/user";

export interface IToken {
  _id: string;
  jwt: string;
  user: IUser;
}

export interface ILogIn {
  password: string;
  email: string;
}

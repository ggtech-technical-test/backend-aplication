import fp from "fastify-plugin";
import { FastifyPluginCallback, FastifyReply, FastifyRequest } from "fastify";
import jwt from "@fastify/jwt";

const jwtConfiguration: FastifyPluginCallback = (fastify, opts, done) => {
  fastify.register(jwt, {
    secret: process.env.JWS_SECRET || "my-secret",
  });

  fastify.decorate(
    "authenticated",
    // @ts-expect-error
    async function (request: FastifyRequest, reply: FastifyReply) {
      try {
        if (!request.headers.authorization) {
          return reply
            .status(401)
            .send({ message: "No authorization header provided" });
        }
        const payload = await request.server.jwt.verify(
          request.headers.authorization as string
        );
        // @ts-expect-error i didn't find a way to properly type the token signature payload
        const userObjId = new request.server.mongo.ObjectId(payload.user_id);
        const user = await request.server.mongo.db
          ?.collection("Users")
          .findOne({
            _id: {
              $eq: userObjId,
            },
          });
        request.user = {
          ...user,
        };
      } catch (error) {
        // @ts-expect-error
        reply.status(error?.statusCode || 400).send({ message: error });
      }
    }
  );

  done();
};

declare module "fastify" {
  interface FastifyInstance {
    authenticated(): Promise<void>;
  }
}
export default fp(jwtConfiguration, { name: "auth-configuration" });

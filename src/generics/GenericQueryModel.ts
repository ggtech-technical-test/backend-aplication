import { FastifyMongoObject } from "@fastify/mongodb";
import { FastifyInstance, FastifyRequest } from "fastify";

export type TPaginationOpts = {
  error?: {
    message: string;
  };
  skip?: number;
  limit?: number;
  hasNextPage?: boolean;
};

export type TPaginationParams = {
  paginated: boolean;
  page?: string;
  count?: string;
};

export default class GenericQueryModel<ModelType> {
  collection: string = "";
  request: FastifyRequest = {} as FastifyRequest;
  mongo: FastifyMongoObject = {} as FastifyMongoObject;
  server: FastifyInstance = {} as FastifyInstance;

  constructor(fastify: FastifyRequest, collectionName: string) {
    this.collection = collectionName;
    this.mongo = fastify.server.mongo;
    this.server = fastify.server;
    this.request = fastify;
  }

  protected getCollection() {
    // @ts-expect-error
    return this.mongo.db?.collection<ModelType>(this.collection);
  }

  protected async getPaginationOpts(): Promise<TPaginationOpts> {
    // @ts-expect-error
    const page = Number(this.request.query.page || 1);
    // @ts-expect-error
    const slice = Number(this.request.query.count || 20);
    const documentsCount = (await this.getCollection()?.countDocuments()) || 0;

    const skip = (page - 1) * slice;
    const lastPage = Math.ceil(documentsCount / slice);
    // when the next slice of data begins in a number higher of documents count
    if (skip > documentsCount) {
      return {
        error: {
          message: `the fraction of data does not exists in ${this.collection}`,
        },
      };
    }
    // if the elements count is higher of the documents count
    if (slice > documentsCount) {
      return {
        error: {
          message: `this ${this.collection} does not have as much as ${slice} documents`,
        },
      };
    }

    return {
      skip: skip,
      limit: slice,
      hasNextPage: lastPage > page,
    };
  }

  protected async get(_id: string) {
    try {
      const collection = this.getCollection();
      const docId = new this.mongo.ObjectId(_id);
      const result = await collection?.findOne({
        _id: { $eq: docId },
      });
      return result;
    } catch (error) {
      this.request.error = `the ${_id} was not found in the ${this.collection}'s documents`;
      return undefined;
    }
  }

  protected async list(filterOpts: object, pagination?: TPaginationParams) {
    try {
      const collection = this.getCollection();
      const aggregationOpts = pagination?.paginated
        ? await this.getPaginationOpts()
        : {};
      if (aggregationOpts.error) {
        this.request.error = aggregationOpts.error.message;
        return undefined;
      }
      const cursor = await collection?.find(
        { ...filterOpts },
        //@ts-expect-error
        { ...aggregationOpts, hasNextPage: undefined }
      );
      return {
        elements: await cursor?.toArray(),
        count: await collection?.estimatedDocumentCount({ ...filterOpts }),
        ...(pagination?.paginated
          ? {
              page: Number(pagination.page || 1),
              count: Number(pagination.count || 20),
              hasNextPage: aggregationOpts.hasNextPage,
            }
          : {}),
      };
    } catch (error) {
      this.request.error = `something came wrong while querying the ${this.collection}'s documents`;
      return undefined;
    }
  }

  protected async aggregation(
    pipelines: object[],
    pagination?: TPaginationParams
  ) {
    try {
      const collection = this.getCollection();
      const aggregationOpts = pagination?.paginated
        ? await this.getPaginationOpts()
        : {};
      if (aggregationOpts.error) {
        this.request.error = aggregationOpts.error.message;
        return undefined;
      }
      //@ts-expect-error
      const cursor = await collection?.aggregate<ModelType>([...pipelines], {
        ...aggregationOpts,
        hasNextPage: undefined,
      });
      return {
        elements: await cursor?.toArray(),
        count: await collection?.estimatedDocumentCount({}),
        ...(pagination?.paginated
          ? {
              page: Number(pagination.page || 1),
              count: Number(pagination.count || 20),
              hasNextPage: aggregationOpts.hasNextPage,
            }
          : {}),
      };
    } catch (error) {
      this.request.error = `something came wrong while querying the ${this.collection}'s documents`;
      return undefined;
    }
  }
}

import { FastifyMongoObject } from "@fastify/mongodb";
import { FastifyInstance, FastifyRequest } from "fastify";

export default class GenericCommandModel<ModelType> {
  collection: string = "";
  request: FastifyRequest = {} as FastifyRequest;
  mongo: FastifyMongoObject = {} as FastifyMongoObject;
  server: FastifyInstance = {} as FastifyInstance;

  constructor(fastify: FastifyRequest, collectionName: string) {
    this.collection = collectionName;
    this.mongo = fastify.server.mongo;
    this.server = fastify.server;
    this.request = fastify;
  }

  protected getCollection() {
    // @ts-expect-error
    return this.mongo?.db?.collection<ModelType>(this.collection);
  }

  protected async create(modelData: Omit<ModelType, "_id">, noAbort?: boolean) {
    try {
      const collection = this.getCollection();
      // @ts-expect-error
      const newInstance = await collection?.insertOne({
        ...(modelData || {}),
        _id: new this.mongo.ObjectId(),
      });
      return newInstance;
    } catch (error) {
      if (noAbort) {
        // @ts-expect-error idk how to type this error handler
        return { ...error, error: true };
      }
      throw new Error(
        `Something came wrong while storing the ${this.collection}'s data`
      );
    }
  }

  protected async delete(_id: string) {
    try {
      const collection = this.getCollection();
      const docId = new this.mongo.ObjectId(_id);
      await collection?.findOneAndDelete({
        _id: {
          $eq: docId,
        },
      });
      return true;
    } catch (error) {
      throw new Error(
        `Something came wrong while deleting ${_id} from ${this.collection}'s data`
      );
    }
  }

  protected async update(modelData: Partial<ModelType>, _id: string) {
    try {
      const collection = this.getCollection();
      const docId = new this.mongo.ObjectId(_id);
      await collection?.findOneAndUpdate(
        {
          _id: {
            $eq: docId,
          },
        },
        {
          $set: {
            ...(modelData || {}),
          },
        }
      );
    } catch (error) {
      throw new Error(
        `Something came wrong while updating ${_id} from ${this.collection}'s data`
      );
    }
  }
}

import MovieCommand from "@models/command/MovieCommand";
import MovieQuery from "@models/query/MovieQuery";
import PlatformQuery from "@models/query/PlatformQuery";
import CategoryQuery from "@models/query/categoryQuery";
import { IMovieCommand } from "@models/types/movie";
import { FastifyReply, FastifyRequest } from "fastify";

// CREATE MOVIE
export async function create(
  request: FastifyRequest<{
    Body: IMovieCommand;
  }>,
  reply: FastifyReply
) {
  const movie = new MovieCommand(request);
  const platforms = new PlatformQuery(request);
  const categories = new CategoryQuery(request);

  const platformObjId = await platforms.checkIds(request.body.platforms);
  if (!platformObjId) {
    return reply.status(400).send({
      message: "some of the platforms for this movie does not exist",
    });
  }
  const categoriesObjId = await categories.checkIds(request.body.categories);
  if (!categoriesObjId) {
    return reply.status(400).send({
      message: "some of the categories for this movie does not exist",
    });
  }
  // Score will always be 0 as it's a new brand movie
  // reviews will be empty too
  const newMovie = await movie.createMovie({
    ...request.body,
    // @ts-expect-error
    platforms: platformObjId,
    // @ts-expect-error
    categories: categoriesObjId,
    score: 0,
    reviews: [],
  });

  return reply.status(201).send({
    data: newMovie,
    message: "movie successfully created",
  });
}

// REMOVE MOVIE
export async function remove(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const movie = new MovieCommand(request);
  const newMovie = await movie.removeMovie(`${request.params.id}`);

  return reply.status(201).send({
    data: newMovie,
    message: "movie successfully removed",
  });
}

// CLONE MOVIE
export async function clone(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const movie = new MovieCommand(request);
  const clonedMovie = await movie.cloneMovie(request.params.id);

  return reply.status(201).send({
    data: clonedMovie,
    message: "movie successfully cloned",
  });
}

// EDIT MOVIE
export async function update(
  request: FastifyRequest<{
    Body: Partial<IMovieCommand>;
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const movie = new MovieCommand(request);

  if (request.body.platforms) {
    const platforms = new PlatformQuery(request);
    const platformObjId = await platforms.checkIds(request.body.platforms);
    if (!platformObjId) {
      return reply.status(400).send({
        message: "some of the platforms for this movie does not exist",
      });
    }
    request.body = {
      ...request.body,
      // @ts-expect-error
      platforms: platformObjId,
    };
  }

  const updatedMovie = await movie.updateMovie(request.body, request.params.id);

  return reply.status(200).send({
    data: updatedMovie,
    message: "movie successfully updated",
  });
}

// SHOW MOVIE
export async function get(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  // todo: add feature to search by slug
  const model = new MovieQuery(request);
  const movie = await model.showMovie(request.params.id);
  if (!request.error) {
    return reply.status(200).send({
      data: movie,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

// LIST MOVIES
export async function list(
  request: FastifyRequest<{
    Querystring: {
      preset?: "populate" | "no-populate";
      paginated?: boolean;
      page?: string;
      count?: string;
    };
  }>,
  reply: FastifyReply
) {
  const model = new MovieQuery(request);
  let movieList: unknown = {};
  if (!request.query.preset || request.query.preset != "populate") {
    movieList = await model.listMovies(
      {},
      {
        paginated: Boolean(request.query.paginated),
        page: request.query.page,
        count: request.query.count,
      }
    );
  }
  if (request.query.preset === "populate") {
    movieList = await model.listPopulatedMovies(
      {},
      {
        paginated: Boolean(request.query.paginated),
        page: request.query.page,
        count: request.query.count,
      }
    );
  }
  if (!request.error) {
    return reply.status(200).send({
      data: movieList,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

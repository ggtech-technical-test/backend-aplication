import { ObjectId } from "@fastify/mongodb";
import ReviewCommand from "@models/command/ReviewCommand";
import MovieQuery from "@models/query/MovieQuery";
import PlatformQuery from "@models/query/PlatformQuery";
import ReviewQuery from "@models/query/ReviewQuery";
import { IReviewCreation } from "@models/types/review";
import { FastifyReply, FastifyRequest } from "fastify";

export async function save(
  request: FastifyRequest<{
    Body: IReviewCreation;
  }>,
  reply: FastifyReply
) {
  const model = new ReviewCommand(request);
  const platforms = new PlatformQuery(request);
  const movie = new MovieQuery(request);

  const movieId = await movie.checkId(request.body.movie);
  if (!movieId) {
    return reply.status(400).send({
      message: "the movie for this review does not exist",
    });
  }
  const platformObjId = await platforms.checkIds([request.body.platform]);
  if (!platformObjId) {
    return reply.status(400).send({
      message: "the platform for this review does not exist",
    });
  }
  await model.updateMovieScore(movieId as ObjectId);
  const review = await model.createReview({
    ...request.body,
    // @ts-expect-error
    user: request.user._id,
    // @ts-expect-error
    author: `${request.user.name} ${request.user.lastName}`,
    // @ts-expect-error
    movie: movieId,
    // @ts-expect-error
    platform: platformObjId[0],
  });

  return reply.status(201).send({
    data: review,
    message: "review successfully created",
  });
}

export async function list(request: FastifyRequest, reply: FastifyReply) {
  const model = new ReviewQuery(request);
  const results = await model.listReview();

  if (!request.error) {
    return reply.status(200).send({
      data: results,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

import PlatformCommand from "@models/command/PlatformCommand";
import PlatformQuery from "@models/query/PlatformQuery";
import IPlatform, { IPlatformCreate } from "@models/types/platform";
import { FastifyReply, FastifyRequest } from "fastify";

// CREATE PLATFORM
export async function create(
  request: FastifyRequest<{
    Body: IPlatformCreate;
  }>,
  reply: FastifyReply
) {
  const model = new PlatformCommand(request);
  const newPlatform = await model.createPlatform({ ...request.body });

  return reply.status(201).send({
    data: newPlatform,
    message: "platform successfully created",
  });
}

// EDIT PLATFORM
export async function update(
  request: FastifyRequest<{
    Body: Partial<IPlatform>;
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const movie = new PlatformCommand(request);
  const updatedPlatform = await movie.updateMovie(
    request.body,
    request.params.id
  );

  return reply.status(200).send({
    data: updatedPlatform,
    message: "platform successfully updated",
  });
}

// REMOVE PLATFORM
export async function remove(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const model = new PlatformCommand(request);
  const platform = await model.removePlatform(`${request.params.id}`);

  return reply.status(201).send({
    data: platform,
    message: "Platform successfully removed",
  });
}

// SHOW PLATFORM
export async function get(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const model = new PlatformQuery(request);
  const platform = await model.showPlatform(request.params.id);
  if (!request.error) {
    return reply.status(200).send({
      data: platform,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

// LIST PLATFORMS
export async function list(request: FastifyRequest, reply: FastifyReply) {
  const model = new PlatformQuery(request);
  const platformList = await model.listPlatforms();

  if (!request.error) {
    return reply.status(200).send({
      data: platformList,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

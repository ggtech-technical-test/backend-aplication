import { FastifyReply, FastifyRequest } from "fastify";
import UserCommand from "@models/command/UserCommand";
import IUser from "@models/types/user";
import UserQuery from "@models/query/UserQuery";

export async function create(
  request: FastifyRequest<{
    Body: Partial<IUser>;
  }>,
  reply: FastifyReply
) {
  const user = new UserCommand(request);
  const newUser = await user.createUser(request.body);
  if (newUser?.error) {
    return reply.status(400).send({
      message: "something came wrong",
    });
  }
  return reply.status(201).send({
    data: newUser,
    message: "successfully created user",
  });
}

export async function listUsers(request: FastifyRequest, reply: FastifyReply) {
  const model = new UserQuery(request);
  return reply.status(200).send({
    data: await model.listUsers(),
  });
}

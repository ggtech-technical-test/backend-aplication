import CategoryCommand from "@models/command/CategoryCommand";
import CategoryQuery from "@models/query/categoryQuery";
import { ICategoryCreate } from "@models/types/category";
import { FastifyReply, FastifyRequest } from "fastify";

// CREATE CATEGORY
export async function create(
  request: FastifyRequest<{
    Body: ICategoryCreate;
  }>,
  reply: FastifyReply
) {
  const model = new CategoryCommand(request);
  const newCategory = await model.createCategory({ ...request.body });

  return reply.status(201).send({
    data: newCategory,
    message: "category successfully created",
  });
}

// REMOVE CATEGORY
export async function remove(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const model = new CategoryCommand(request);
  const category = await model.removeCategory(`${request.params.id}`);

  return reply.status(201).send({
    data: category,
    message: "Category successfully removed",
  });
}

// SHOW CATEGORY
export async function get(
  request: FastifyRequest<{
    Params: {
      id: string;
    };
  }>,
  reply: FastifyReply
) {
  const model = new CategoryQuery(request);
  const category = await model.showCategory(request.params.id);
  if (!request.error) {
    return reply.status(200).send({
      data: category,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

// LIST CATEGORIES
export async function list(request: FastifyRequest, reply: FastifyReply) {
  const model = new CategoryQuery(request);
  const categoriesList = await model.listCategories();

  if (!request.error) {
    return reply.status(200).send({
      data: categoriesList,
    });
  }
  return reply.status(404).send({
    data: undefined,
    message: request.error,
  });
}

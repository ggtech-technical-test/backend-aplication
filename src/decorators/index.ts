import {
  FastifyInstance,
  FastifyPluginOptions,
  HookHandlerDoneFunction,
} from "fastify";
import fp from "fastify-plugin";
import requestDecorator from "./requestDecorators";

export default fp(
  function decoratorsRegister(
    fastify: FastifyInstance,
    opts: FastifyPluginOptions,
    done: HookHandlerDoneFunction
  ) {
    fastify.register(fp(requestDecorator));
    done();
  },
  { name: "decorators-register-plugin" }
);

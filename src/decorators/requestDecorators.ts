import {
  FastifyInstance,
  FastifyPluginOptions,
  HookHandlerDoneFunction,
} from "fastify";

export default function requestDecorator(
  fastify: FastifyInstance,
  opts: FastifyPluginOptions,
  done: HookHandlerDoneFunction
) {
  fastify.decorateRequest("error", "");
  done();
}

declare module "fastify" {
  interface FastifyRequest {
    error: string;
  }
}

import { FastifyPluginCallback } from "fastify";
import { create, remove, get, list } from "@controllers/CategoryController";
import { categoryCreationSchema } from "@schema/category";

const platformRoutes: FastifyPluginCallback = (fastify, opts, done) => {
  // CREATE PLATFORM
  fastify.route({
    method: "POST",
    handler: create,
    url: "/category/create",
    onRequest: [fastify.authenticated],
    schema: categoryCreationSchema,
  });

  // REMOVE PLATFORM
  fastify.route({
    method: "DELETE",
    handler: remove,
    url: "/category/:id",
    onRequest: [fastify.authenticated],
  });

  // SHOW PLATFORM
  fastify.route({
    method: "GET",
    handler: get,
    url: "/category/:id",
    // onRequest: [fastify.authenticated],
  });

  // LIST PLATFORMS
  fastify.route({
    method: "GET",
    handler: list,
    url: "/category",
    // onRequest: [fastify.authenticated],
  });

  done();
};

export default platformRoutes;

import { FastifyPluginCallback } from "fastify";
import {
  create,
  remove,
  update,
  get,
  list,
} from "@controllers/PlatformController";
import { platformCreateSchema } from "@schema/platform";

const platformRoutes: FastifyPluginCallback = (fastify, opts, done) => {
  // CREATE PLATFORM
  fastify.route({
    method: "POST",
    handler: create,
    url: "/platform/create",
    onRequest: [fastify.authenticated],
    schema: platformCreateSchema,
  });

  // REMOVE PLATFORM
  fastify.route({
    method: "DELETE",
    handler: remove,
    url: "/platform/:id",
    onRequest: [fastify.authenticated],
  });

  // EDIT PLATFORM
  fastify.route({
    method: "PATCH",
    handler: update,
    url: "/platform/:id",
    onRequest: [fastify.authenticated],
  });

  // SHOW PLATFORM
  fastify.route({
    method: "GET",
    handler: get,
    url: "/platform/:id",
    // onRequest: [fastify.authenticated],
  });

  // LIST PLATFORMS
  fastify.route({
    method: "GET",
    handler: list,
    url: "/platform",
    // onRequest: [fastify.authenticated],
  });

  done();
};

export default platformRoutes;

import { FastifyPluginCallback } from "fastify";
import {
  create,
  clone,
  remove,
  update,
  get,
  list,
} from "@controllers/MovieController";
import { movieCreateSchema } from "@schema/movie";

const movieRoutes: FastifyPluginCallback = (fastify, opts, done) => {
  // CREATE MOVIE
  fastify.route({
    method: "POST",
    handler: create,
    url: "/movie/create",
    onRequest: [fastify.authenticated],
    schema: movieCreateSchema,
  });

  // REMOVE MOVIE
  fastify.route({
    method: "DELETE",
    handler: remove,
    url: "/movie/:id",
    onRequest: [fastify.authenticated],
    // TODO: validation schema
    // schema
  });

  // EDIT MOVIE
  fastify.route({
    method: "PATCH",
    handler: update,
    url: "/movie/:id",
    onRequest: [fastify.authenticated],
    // TODO: validation schema
    // schema
  });

  // CLONE MOVIE
  fastify.route({
    method: "POST",
    handler: clone,
    url: "/movie/clone/:id",
    onRequest: [fastify.authenticated],
    // TODO: validation schema
    // schema
  });

  // SHOW MOVIE
  fastify.route({
    method: "GET",
    handler: get,
    url: "/movie/:id",
    // onRequest: [fastify.authenticated],
    // TODO: validation schema
  });

  // LIST MOVIES
  fastify.route({
    method: "GET",
    handler: list,
    url: "/movie",
    // onRequest: [fastify.authenticated],
    // TODO: validation schema
  });

  done();
};

export default movieRoutes;

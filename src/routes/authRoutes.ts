import { logIn } from "auth/controllers/AuthController";
import { loginSchema } from "auth/controllers/schema";
import {
  FastifyInstance,
  HookHandlerDoneFunction,
  RouteOptions,
} from "fastify";

export function authRoutes(
  fastify: FastifyInstance,
  opts: RouteOptions,
  done: HookHandlerDoneFunction
) {
  fastify.route({
    method: "POST",
    schema: loginSchema,
    handler: logIn,
    url: "/login",
  });
  done();
}

export default authRoutes;

import { create, listUsers } from "@controllers/UserController";
import { userCreateSchema } from "@schema/user";
import {
  FastifyInstance,
  HookHandlerDoneFunction,
  RouteOptions,
} from "fastify";

export function userRoutes(
  fastify: FastifyInstance,
  opts: RouteOptions,
  done: HookHandlerDoneFunction
) {
  fastify.route({
    method: "POST",
    schema: userCreateSchema,
    handler: create,
    url: "/user/create",
  });
  fastify.route({
    method: "GET",
    handler: listUsers,
    onRequest: [fastify.authenticated],
    url: "/user",
  });
  done();
}

export default userRoutes;

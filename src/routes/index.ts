import fp from "fastify-plugin";
import { FastifyPluginCallback, FastifyReply, FastifyRequest } from "fastify";
import user from "@routes/userRoutes";
import movie from "@routes/movieRoutes";
import platform from "@routes/platformRoutes";
import review from "@routes/reviewRoutes";
import category from "@routes/categoryRoutes";
import auth from "@routes/authRoutes";

const routesPlugin: FastifyPluginCallback = async (fastify, opts, done) => {
  fastify.register(auth);
  fastify.register(user);
  fastify.register(category);
  fastify.register(platform);
  fastify.register(movie);
  fastify.register(review);
  fastify.get("/service", (request: FastifyRequest, reply: FastifyReply) => {
    reply.status(200).send({
      message: "Yes! it's running the API",
    });
  });
  done();
};

export default fp(routesPlugin, {
  name: "routes-loader",
});

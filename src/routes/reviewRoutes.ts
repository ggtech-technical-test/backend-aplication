import { list, save } from "@controllers/ReviewController";
import { reviewCreateSchema } from "@schema/review";
import {
  FastifyInstance,
  HookHandlerDoneFunction,
  RouteOptions,
} from "fastify";

export function reviewRoutes(
  fastify: FastifyInstance,
  opts: RouteOptions,
  done: HookHandlerDoneFunction
) {
  fastify.route({
    method: "POST",
    schema: reviewCreateSchema,
    handler: save,
    onRequest: [fastify.authenticated],
    url: "/review/create",
  });
  fastify.route({
    method: "GET",
    handler: list,
    onRequest: [fastify.authenticated],
    url: "/review",
  });
  done();
}

export default reviewRoutes;

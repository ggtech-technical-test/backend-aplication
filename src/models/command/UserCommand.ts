import GenericCommandModel from "@generics/GenericCommandModel";
import { FastifyRequest } from "fastify";
import IUser from "@models/types/user";
import { hash } from "bcrypt";

export default class UserCommand extends GenericCommandModel<IUser> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Users");
  }

  public async createUser(data: Partial<IUser>) {
    const password = await hash(data.password!, 5);
    const newUser = await this.create(
      {
        ...data,
        password,
        createdAt: new Date(),
        updatedAt: new Date(),
      } as IUser,
      true
    );
    return newUser;
  }

  public async removeUser(_id: string) {
    const removedUser = await this.delete(_id);
    return removedUser;
  }
}

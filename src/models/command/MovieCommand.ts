import GenericCommandModel from "@generics/GenericCommandModel";
import MovieQuery from "@models/query/MovieQuery";
import IMovie, { IMovieCommand } from "@models/types/movie";
import { FastifyRequest } from "fastify";
import slugify from "slugify";

export default class MovieCommand extends GenericCommandModel<IMovie> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Movies");
  }

  // CREATE
  public async createMovie(data: Partial<IMovie>) {
    const newMovie = await this.create({
      ...data,
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: slugify(data.title!.toLowerCase()),
    } as IMovie);
    return newMovie;
  }

  // DELETE
  public async removeMovie(_id: string) {
    const deletedMovie = await this.delete(_id);
    return deletedMovie;
  }

  // CLONE
  public async cloneMovie(_id: string) {
    const query = new MovieQuery(this.request);
    const movie = await query.showMovie(_id);
    const clonedMovie: Omit<IMovie, "_id" | "createdAt" | "updatedAt"> = {
      director: movie!.director,
      image: movie!.image,
      platforms: movie!.platforms,
      categories: movie!.categories,
      score: movie!.score,
      slug: movie!.slug,
      title: movie!.title,
      reviews: movie!.reviews,
    };
    return await this.createMovie(clonedMovie);
  }

  // UPDATE
  public async updateMovie(data: Partial<IMovieCommand>, _id: string) {
    const movie = await this.update(
      { ...data, updatedAt: new Date() } as Partial<IMovie>,
      _id
    );
    return movie;
  }
}

import GenericCommandModel from "@generics/GenericCommandModel";
import IPlatform from "@models/types/platform";
import { FastifyRequest } from "fastify";

export default class PlatformCommand extends GenericCommandModel<IPlatform> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Platforms");
  }

  // CREATE
  public async createPlatform(data: Partial<IPlatform>) {
    const newPlatform = await this.create({
      ...data,
      createdAt: new Date(),
      updatedAt: new Date(),
    } as IPlatform);
    return newPlatform;
  }

  // DELETE
  public async removePlatform(_id: string) {
    const deletePlatform = await this.delete(_id);
    return deletePlatform;
  }

  // UPDATE
  public async updateMovie(data: Partial<IPlatform>, _id: string) {
    const platform = await this.update(
      {
        ...data,
        updatedAt: new Date(),
      },
      _id
    );
    return platform;
  }
}

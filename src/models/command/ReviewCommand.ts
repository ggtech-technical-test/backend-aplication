import { ObjectId } from "@fastify/mongodb";
import GenericCommandModel from "@generics/GenericCommandModel";
import IReview, { IReviewCreation } from "@models/types/review";
import { FastifyRequest } from "fastify";
import MovieCommand from "./MovieCommand";
import ReviewQuery from "@models/query/ReviewQuery";

export default class ReviewCommand extends GenericCommandModel<IReview> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Reviews");
  }

  public async updateMovieScore(movieId: ObjectId) {
    const query = new ReviewQuery(this.request);
    const movieModel = new MovieCommand(this.request);
    const { average, reviews } = await query.listMovieReviews(movieId);
    await movieModel.updateMovie(
      {
        score: average,
        // @ts-expect-error
        reviews: reviews,
      },
      movieId.toString()
    );
  }

  // CREATE
  public async createReview(data: IReviewCreation) {
    const review = await this.create({
      ...data,
      createdAt: new Date(),
      updatedAt: new Date(),
    } as unknown as IReview);

    return review;
  }
}

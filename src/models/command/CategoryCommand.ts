import GenericCommandModel from "@generics/GenericCommandModel";
import ICategory from "@models/types/category";
import { FastifyRequest } from "fastify";

export default class CategoryCommand extends GenericCommandModel<ICategory> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Categories");
  }

  // CREATE
  public async createCategory(data: Partial<ICategory>) {
    const newCategory = await this.create({
      ...data,
      createdAt: new Date(),
      updatedAt: new Date(),
    } as ICategory);
    return newCategory;
  }

  // DELETE
  public async removeCategory(_id: string) {
    const deleteCategory = await this.delete(_id);
    return deleteCategory;
  }
}

export default interface IUser {
  _id?: string;
  role: number;
  name: string;
  lastName: string;
  password: string;
  email: string;
  createdAt: Date; // Fecha de creación
  updatedAt: Date; // Fecha de actualización
}

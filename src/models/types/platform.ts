export default interface IPlatform {
  _id: string; // ID de la plataforma.
  title: string; // Nombre de la plataforma. Ejemplo: Netflix
  createdAt: Date; // Fecha de creación de la plataforma.
  updatedAt: Date; // Fecha de última actualización de la plataforma.
  // TODO: implement images url and storage
  icon: string; // Icono de la plataforma. Ejemplo: netflix-icon.jpg
  // image: string
}

export type IPlatformCreate = Omit<
  IPlatform,
  "_id" | "createdAt" | "updatedAt"
>;

import IMovie from "./movie";
import IPlatform from "./platform";
import IUser from "./user";

export default interface IReview {
  _id: string; // ID de la reseña.
  movie: IMovie; // ID de la película sobre la que se va a reseñar.
  user: IUser; // ID del usuario que esribio la reseña
  platform: IPlatform; // ID de la plataforma sobre la que se va a reseñar.
  author: string; // Nombre del autor o usuario que está creando la reseña.
  body: string; // Texto de la reseña.
  score: number; // Calificación 0 a 5 de la reseña.
  createdAt: Date; // Fecha de creación de la reseña.
  updatedAt: Date; // Fecha de actualización de la reseña.
}

export interface IReviewCreation {
  movie: string;
  platform: string;
  body: string;
  score: number;
}

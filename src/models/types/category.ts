export default interface ICategory {
  _id: string;
  title: string;
  createdAt: Date;
  updatedAt: Date;
}

export type ICategoryCreate = Omit<
  ICategory,
  "_id" | "createdAt" | "updatedAt"
>;

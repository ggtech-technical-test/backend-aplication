import { ObjectId } from "@fastify/mongodb";
import GenericQueryModel, {
  TPaginationParams,
} from "@generics/GenericQueryModel";
import IMovie from "@models/types/movie";
import { FastifyRequest } from "fastify";

export default class MovieQuery extends GenericQueryModel<IMovie> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Movies");
  }

  public async showMovie(_id: string) {
    const movie = await this.get(_id);
    // todo: include population and slug-search by
    return movie;
  }

  public async listMovies(filters: object, pagination?: TPaginationParams) {
    return await this.list(filters, pagination);
  }

  public async listPopulatedMovies(
    filters: object,
    pagination?: TPaginationParams
  ) {
    return await this.aggregation(
      [
        {
          $lookup: {
            from: "Reviews",
            localField: "reviews",
            foreignField: "_id",
            as: "reviews",
            pipeline: [
              {
                $lookup: {
                  from: "Platforms",
                  localField: "platform",
                  foreignField: "_id",
                  as: "platform",
                },
              },
              {
                $unwind: "$platform",
              },
              {
                $group: {
                  _id: "$platform.title",
                  items: {
                    $addToSet: "$$CURRENT",
                  },
                },
              },
            ],
          },
        },
        {
          $lookup: {
            from: "Platforms",
            localField: "platforms",
            foreignField: "_id",
            as: "platforms",
          },
        },
        {
          $lookup: {
            from: "Categories",
            localField: "categories",
            foreignField: "_id",
            as: "categories",
          },
        },
      ],
      pagination
    );
  }

  public async checkId(_id: string): Promise<boolean | ObjectId> {
    try {
      const movie = await this.get(_id);
      if (movie) {
        return movie._id as unknown as ObjectId;
      }
      return false;
    } catch (error) {
      return false;
    }
  }
}

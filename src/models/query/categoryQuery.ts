import { ObjectId } from "@fastify/mongodb";
import GenericQueryModel from "@generics/GenericQueryModel";
import ICategory from "@models/types/category";
import { FastifyRequest } from "fastify";

export default class CategoryQuery extends GenericQueryModel<ICategory> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Categories");
  }

  // SHOW
  public async showCategory(_id: string) {
    const Category = await this.get(_id);
    return Category;
  }

  public async listCategories() {
    return await this.list({});
  }

  public async checkIds(_ids: string[]): Promise<boolean | ObjectId[]> {
    try {
      const objectsIds = _ids.map((id) => new this.mongo.ObjectId(id));
      const category = await this.list({
        _id: {
          $in: objectsIds,
        },
      });
      if (category!.elements!.length > 0) {
        return objectsIds;
      }
      return false;
    } catch (error) {
      return false;
    }
  }
}

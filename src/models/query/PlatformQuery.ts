import { ObjectId } from "@fastify/mongodb";
import GenericQueryModel from "@generics/GenericQueryModel";
import IPlatform from "@models/types/platform";
import { FastifyRequest } from "fastify";

export default class PlatformQuery extends GenericQueryModel<IPlatform> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Platforms");
  }

  // SHOW
  public async showPlatform(_id: string) {
    const platform = await this.get(_id);
    return platform;
  }

  public async listPlatforms() {
    return await this.list({});
  }

  public async checkIds(_ids: string[]): Promise<boolean | ObjectId[]> {
    try {
      const objectsIds = _ids.map((id) => new this.mongo.ObjectId(id));
      const platforms = await this.list({
        _id: {
          $in: objectsIds,
        },
      });
      if (platforms!.elements!.length > 0) {
        return objectsIds;
      }
      return false;
    } catch (error) {
      return false;
    }
  }
}

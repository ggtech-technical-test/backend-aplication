import { ObjectId } from "@fastify/mongodb";
import GenericQueryModel from "@generics/GenericQueryModel";
import IReview from "@models/types/review";
import { FastifyRequest } from "fastify";

export default class ReviewQuery extends GenericQueryModel<IReview> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Reviews");
  }

  public async listMovieReviews(
    movieId: ObjectId
  ): Promise<{ average: number; reviews: ObjectId[] }> {
    const results = await this.list({
      movie: {
        $eq: movieId,
      },
    });
    if (!this.request.error)
      return {
        average: Number(
          (
            results!.elements!.reduce(
              (previous: number, review: IReview) => previous + review.score,
              0
            ) / (results?.count || 1)
          ).toFixed(2)
        ),
        reviews: results!.elements!.map(
          (review) => review._id as unknown as ObjectId
        ),
      };
    throw new Error("Something came wrong while querying the movie's reviews");
  }

  public async listReview() {
    try {
      const collection = this.getCollection();
      const cursor = await collection?.aggregate<IReview>([
        {
          $lookup: {
            from: "Movies",
            localField: "movie",
            foreignField: "_id",
            as: "movie",
          },
        },
        {
          $lookup: {
            from: "Platforms",
            localField: "platform",
            foreignField: "_id",
            as: "platform",
          },
        },
      ]);
      return {
        elements: await cursor?.toArray(),
        count: await collection?.estimatedDocumentCount({}),
      };
    } catch (error) {
      this.request.error = `something came wrong while querying the ${this.collection}'s documents`;
      return undefined;
    }
  }
}

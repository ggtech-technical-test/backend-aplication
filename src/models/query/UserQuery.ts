import GenericQueryModel from "@generics/GenericQueryModel";
import IUser from "@models/types/user";
import { compare } from "bcrypt";
import { FastifyRequest } from "fastify";

export default class UserQuery extends GenericQueryModel<IUser> {
  constructor(fastify: FastifyRequest) {
    super(fastify, "Users");
  }

  public async checkCredentials(
    email: string,
    password: string
  ): Promise<boolean | IUser> {
    try {
      const result = await this.getCollection()?.findOne({
        email: { $eq: email },
      });
      if (result) {
        const granted = await compare(password, result.password);
        if (granted) {
          return result;
        }
        return false;
      }
      return false;
    } catch (error) {
      return false;
    }
  }

  public async listUsers() {
    return await this.list({});
  }

  public async showUser(_id: string) {
    const movie = await this.get(_id);
    return movie;
  }
}

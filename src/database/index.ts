import { FastifyPluginCallback } from "fastify";
import plugin from "fastify-plugin";
import mongoDbPlugin from "@fastify/mongodb";

const databaseConnection: FastifyPluginCallback = async (
  fastify,
  opts,
  done
) => {
  await fastify.register(mongoDbPlugin, {
    forceClose: true,
    url: process.env.MONGO_URL,
  });
  // creating unique index in users for email field
  fastify.mongo.db
    ?.collection("Users")
    .createIndex({ email: 1 }, { unique: true });

  done();
};

export default plugin(databaseConnection, { name: "database-connection" });

import { FastifySchema } from "fastify";

export const userCreateSchema: FastifySchema = {
  body: {
    type: "object",
    required: ["name", "lastName", "password", "email"],
    properties: {
      name: {
        type: "string",
        maxLength: 25,
      },
      lastName: {
        type: "string",
        maxLength: 25,
      },
      password: {
        type: "string",
        maxLength: 50,
      },
      email: {
        type: "string",
        maxLength: 20,
      },
    },
  },
};

import { FastifySchema } from "fastify";

export const categoryCreationSchema: FastifySchema = {
  body: {
    type: "object",
    required: ["title"],
    properties: {
      title: {
        type: "string",
        maxLength: 50,
      },
    },
  },
};

import { FastifySchema } from "fastify";

export const reviewCreateSchema: FastifySchema = {
  body: {
    type: "object",
    required: ["movie", "platform", "body", "score"],
    properties: {
      movie: {
        type: "string",
        maxLength: 50,
      },
      platform: {
        type: "string",
        maxLength: 50,
      },
      body: {
        type: "string",
        maxLength: 250,
      },
      score: {
        type: "integer",
        maximum: 5,
        minimum: 1,
      },
    },
  },
};

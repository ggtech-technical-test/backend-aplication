import { FastifySchema } from "fastify";

export const platformCreateSchema: FastifySchema = {
  body: {
    type: "object",
    required: ["title", "icon"],
    properties: {
      title: {
        type: "string",
        maxLength: 50,
      },
      // TODO: change this to file type
      icon: {
        type: "string",
        maxLength: 50,
      },
    },
  },
};

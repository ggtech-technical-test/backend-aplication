import { FastifySchema } from "fastify";

export const movieCreateSchema: FastifySchema = {
  body: {
    type: "object",
    required: ["title", "image", "director", "platforms", "categories"],
    properties: {
      title: {
        type: "string",
        maxLength: 50,
      },
      // TODO: change this to file type
      image: {
        type: "string",
        maxLength: 50,
      },
      director: {
        type: "string",
        maxLength: 50,
      },
      platforms: {
        type: "array",
        items: {
          type: "string",
        },
        // FIXME: this is an arbitrary value, the max it's the amount of platforms registered
        maxItems: 5,
      },
      categories: {
        type: "array",
        items: {
          type: "string",
        },
        maxItems: 2,
      },
    },
  },
};

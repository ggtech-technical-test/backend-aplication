// Import the framework and instantiate it
import "tsconfig-paths/register";
import Fastify from "fastify";
import database from "database";
import routes from "routes";
import decorators from "decorators";
import auth from "auth";
import cors from "@fastify/cors";
import "dotenv/config";

const fastify = Fastify({
  logger: {
    level: "info",
  },
});

// database connection
fastify.register(database);

// cors configuration
fastify.register(cors, {
  origin: "*",
  methods: ["GET", "POST", "PATCH", "DELETE"],
});

// authentication configuration
fastify.register(auth);

// routes declaration
fastify.register(routes);

// decorators
fastify.register(decorators);

// Run the server!
async function runServer() {
  try {
    await fastify.listen(
      {
        port: Number(process.env.SERVER_PORT || 8081),
        host: process.env.SERVER_HOST || "127.0.0.1",
      },
      (err, address) => {
        if (err) {
          fastify.log.info(err);
          process.exit(1);
        }
        fastify.log.info(`API listening at ${address}`);
      }
    );
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
}
runServer();
